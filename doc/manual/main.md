![node-red-contrib-mydb logo](node-red-contrib-mydb.png)

This package provides two node-red nodes:
* An input/output node for DB queries
* A DB connection configuration node

# Links

 * Project homepage: [hosted on GitLab Pages](
   https://open-kappa.gitlab.io/node-red/node-red-contrib-mydb)

 * Project sources: [hosted on gitlab.com](
   https://gitlab.com/open-kappa/node-red/node-red-contrib-mydb)

 * List of open-kappa projects, [hosted on GitLab Pages](
   https://open-kappa.gitlab.io)

# License

*@open-kappa/node-red-contrib-mydb* is released under the liberal MIT
License. Please refer to the LICENSE.txt project file for further details.

# Patrons

This node-red module has been sponsored by [Gizero Energie s.r.l.](
www.gizeroenergie.it).
