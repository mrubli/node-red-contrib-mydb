
function addAlias(dictionary)
{
    dictionary.lookUp("description").synonym("brief");
    dictionary.lookUp("param").synonym("typeParam");
};

exports.defineTags = addAlias;
