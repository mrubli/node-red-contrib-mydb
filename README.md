# @open-kappa/node-red-contrib-mydb

This package provides two node-red nodes:
* An input/output node for DB queries
* A DB connection configuration node

At the moment only Postgres is supported, but maybe other DB's will be supported in the future.

# Links

 * Project homepage: [hosted on GitLab Pages](
   https://open-kappa.gitlab.io/node-red/node-red-contrib-mydb)

 * Project sources: [hosted on gitlab.com](
   https://gitlab.com/open-kappa/node-red/node-red-contrib-mydb)

 * List of open-kappa projects, [hosted on GitLab Pages](
   https://open-kappa.gitlab.io)

# License

*@open-kappa/node-red-contrib-mydb* is released under the liberal MIT License.
Please refer to the LICENSE.txt project file for further details.

# Credits

All the stuff into this package as been created from scratch, with the following
exceptions:

* The node icon has been taken from the original *postgrestor* node.
* The node HTML has been takend and adapted from the original *postgrestor* node.

# Patrons

This node-red module has been sponsored by [Gizero Energie s.r.l.](
www.gizeroenergie.it).

# Contributors

* Francesco Stefanni
* Martin Rubli
