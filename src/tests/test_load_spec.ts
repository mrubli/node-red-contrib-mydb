import {
    IFlowNode,
    MyNodeRedTest
} from "@open-kappa/mytest";
import mydbConfigInit
    from "../node-red-contrib-mydb/node-red-contrib-mydbConfig";
import mydbInit
    from "../node-red-contrib-mydb/node-red-contrib-mydb";
import myDbRunMode
    from "../node-red-contrib-mydb/src/MyDbRunMode";


myDbRunMode.setTesting(true);


class ThisTest extends MyNodeRedTest
{
    constructor()
    {
        super("mydb load test", [mydbInit, mydbConfigInit]);
    }

    protected registerTests(): void
    {
        const self = this;
        self.registerTest(
            "Should load",
            self._testLoad
        );
    }

    protected registerFlows(): void
    {
        const self = this;
        self.registerFlow(
            "Should load",
            self._flowLoad
        );
    }

    private _flowLoad(): Array<IFlowNode>
    {
        const self = this;
        return [
            self.makeNode("nodeCfg", "nodeConfig1", "mydbConfig"),
            self.makeNode("n1", "node1", "mydb", {"mydbConfig": "nodeCfg"})
        ];
    }

    private _testLoad(): void
    {
        const self = this;
        const n1 = self.getNode("n1");
        n1.should.have.property("name", "node1");
    }
}

const test = new ThisTest();
test.run();
