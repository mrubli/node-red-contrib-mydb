import "should";
import {
    IFlowNode,
    MyNodeRedTest
} from "@open-kappa/mytest";
import mydbConfigInit
    from "../node-red-contrib-mydb/node-red-contrib-mydbConfig";
import mydbInit
    from "../node-red-contrib-mydb/node-red-contrib-mydb";
import myDbRunMode
    from "../node-red-contrib-mydb/src/MyDbRunMode";

myDbRunMode.setTesting(true);

class ThisTest extends MyNodeRedTest
{
    constructor()
    {
        super("mydb substitution test", [mydbInit, mydbConfigInit]);
    }

    protected registerTests(): void
    {
        const self = this;
        self.registerAsyncTest(
            "Should substitute",
            self._testSubstitution
        );
        self.registerAsyncTest(
            "Should substitute Mustache",
            self._testSubstituteMustache
        );
        self.registerAsyncTest(
            "Should substitute Handlebars",
            self._testSubstituteHandlebars
        );
        self.registerAsyncTest(
            "Should substitute env vars",
            self._testSubstituteEnvVars
        );
    }

    protected registerFlows(): void
    {
        const self = this;
        self.registerFlow(
            "Should substitute",
            self._flowSubstitution
        );
        self.registerFlow(
            "Should substitute Mustache",
            self._flowSubstituteMustache
        );
        self.registerFlow(
            "Should substitute Handlebars",
            self._flowSubstituteHandlebars
        );
        self.registerFlow(
            "Should substitute env vars",
            self._flowSubstituteEnvVars
        );
    }

    private _flowSubstitution(): Array<IFlowNode>
    {
        const self = this;

        const opts = {
            "mydbConfig": "nodeCfg",
            "query": "SELECT * FROM {{msg.tableName}};"
        };

        const cfgNode = self.makeNode("nodeCfg", "nodeCfg1", "mydbConfig");
        let ret = [cfgNode];

        /* eslint-disable function-call-argument-newline */
        ret = ret.concat(
            self.makeNodeWithSourceAndSink(
                "n0", "node0",
                "n1", "node1", "mydb",
                "n2", "node2",
                opts
            )
        );
        /* eslint-enable function-call-argument-newline */

        return ret;
    }

    private _testSubstitution(done: (err?: Error) => void): void
    {
        const self = this;
        const n1 = self.getNode("n1");
        const n2 = self.getNode("n2");
        function checkResult(msg: any): void
        {
            try
            {
                const query = msg.payload.command;
                const result = query.indexOf("}") === -1
                    && query.indexOf("test_table") !== -1;
                result.should.be.true("Substitution does not work");
                done();
            }
            catch (error)
            {
                done(error);
            }
        }
        n2.on("input", checkResult);
        const msg: any = {
            "tableName": "test_table"
        };
        n1.receive(msg);
    }

    private _flowSubstituteMustache(): Array<IFlowNode>
    {
        const self = this;

        const opts = {
            "mydbConfig": "nodeCfg",
            // This query contains a lambda, which Mustache supports but Handlebars does not:
            "query": "SELECT '{{#msg.boldify}}Important table: {{msg.tableName}}{{/msg.boldify}}';",
            "style": "mustache"
        };

        const cfgNode = self.makeNode("nodeCfg", "nodeCfg1", "mydbConfig");
        let ret = [cfgNode];

        /* eslint-disable function-call-argument-newline */
        ret = ret.concat(
            self.makeNodeWithSourceAndSink(
                "n0", "node0",
                "n1", "node1", "mydb",
                "n2", "node2",
                opts
            )
        );
        /* eslint-enable function-call-argument-newline */

        return ret;
    }

    private _testSubstituteMustache(done: (err?: Error) => void): void
    {
        const self = this;
        const n1 = self.getNode("n1");
        const n2 = self.getNode("n2");
        function checkResult(msg: any): void
        {
            try
            {
                const query = msg.payload.command;
                query.should.equal("SELECT '<b>Important table: foobar</b>';");
                done();
            }
            catch (error)
            {
                done(error);
            }
        }
        n2.on("input", checkResult);
        const msg: any = {
            "tableName": "foobar",
            "boldify":      function() {
                return (text: any, render: any) => `<b>${render(text)}</b>`;
            }
        };
        n1.receive(msg);
    }

    private _flowSubstituteHandlebars(): Array<IFlowNode>
    {
        const self = this;

        const opts = {
            "mydbConfig": "nodeCfg",
            // This query uses the @last variable which Handlebars supports but Mustache does not
            "query": `INSERT INTO mytable
                        VALUES
                          {{#each msg.values}}
                          ('{{timestamp}}', {{temperature}}, {{humidity}}){{^if @last}},{{/if}}
                          {{/each}}
                      ;`,
            "style": "handlebars"
        };

        const cfgNode = self.makeNode("nodeCfg", "nodeCfg1", "mydbConfig");
        let ret = [cfgNode];

        /* eslint-disable function-call-argument-newline */
        ret = ret.concat(
            self.makeNodeWithSourceAndSink(
                "n0", "node0",
                "n1", "node1", "mydb",
                "n2", "node2",
                opts
            )
        );
        /* eslint-enable function-call-argument-newline */

        return ret;
    }

    private _testSubstituteHandlebars(done: (err?: Error) => void): void
    {
        const self = this;
        const n1 = self.getNode("n1");
        const n2 = self.getNode("n2");
        function checkResult(msg: any): void
        {
            try
            {
                const query = msg.payload.command;
                query.should.equal(
                     `INSERT INTO mytable
                        VALUES
                          ('2020-08-31 00:00:00+00', 20, 80),
                          ('2020-08-31 00:00:01+00', 21, 79),
                          ('2020-08-31 00:00:02+00', 22, 78)
                      ;`
                )
                done();
            }
            catch (error)
            {
                done(error);
            }
        }
        n2.on("input", checkResult);
        const msg: any = {
            "values": [
                {
                    "timestamp": "2020-08-31 00:00:00+00",
                    "temperature": 20,
                    "humidity": 80
                },
                {
                    "timestamp": "2020-08-31 00:00:01+00",
                    "temperature": 21,
                    "humidity": 79
                },
                {
                    "timestamp": "2020-08-31 00:00:02+00",
                    "temperature": 22,
                    "humidity": 78
                },
            ],
        };
        n1.receive(msg);
    }

    private _flowSubstituteEnvVars(): Array<IFlowNode>
    {
        const self = this;

        process.env.FOO = "bar";
        const opts = {
            "mydbConfig": "nodeCfg",
            "substEnvVars": true,
            "query": "SELECT '${FOO}';"
        };

        const cfgNode = self.makeNode("nodeCfg", "nodeCfg1", "mydbConfig");
        let ret = [cfgNode];

        /* eslint-disable function-call-argument-newline */
        ret = ret.concat(
            self.makeNodeWithSourceAndSink(
                "n0", "node0",
                "n1", "node1", "mydb",
                "n2", "node2",
                opts
            )
        );
        /* eslint-enable function-call-argument-newline */

        return ret;
    }

    private _testSubstituteEnvVars(done: (err?: Error) => void): void
    {
        const self = this;
        const n1 = self.getNode("n1");
        const n2 = self.getNode("n2");
        function checkResult(msg: any): void
        {
            try
            {
                const query = msg.payload.command;
                query.should.equal("SELECT 'bar';");
                done();
            }
            catch (error)
            {
                done(error);
            }
        }
        n2.on("input", checkResult);
        const msg: any = {
        };
        n1.receive(msg);
    }
}


const test = new ThisTest();
test.run();
