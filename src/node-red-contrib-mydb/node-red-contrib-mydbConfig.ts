/**
 * The main module.
 * It exports the function mydbConfigInit().
 * @module node-red-contrib-mydb
 */
import {
    getMyBuilders,
    IMyUserParams
} from "@open-kappa/node-red-contrib-myutils";
import {
    MyDbConfig
} from "./src/mydbImpl";


const mydbNodes: Array<IMyUserParams> = [
    {
        "UserClass": MyDbConfig,
        "name": "mydbConfig",
        "opts": {
            "credentials": {
                "password": {
                    "type": "password"
                },
                "user": {
                    "type": "text"
                }
            }
        }
    }
];


const mydbConfigInit = getMyBuilders(mydbNodes);

module.exports = mydbConfigInit;
export default mydbConfigInit;
