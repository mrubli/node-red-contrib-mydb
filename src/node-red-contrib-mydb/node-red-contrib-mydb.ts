/**
 * The main module.
 * It exports the function mydbInit().
 * @module node-red-contrib-mydb
 */
import {
    getMyBuilders,
    IMyUserParams
} from "@open-kappa/node-red-contrib-myutils";
import {
    MyDb
} from "./src/mydbImpl";


const mydbNodes: Array<IMyUserParams> = [
    {
        "UserClass": MyDb,
        "name": "mydb"
    }
];


const mydbInit = getMyBuilders(mydbNodes);

module.exports = mydbInit;
export default mydbInit;
