/**
 * The MyDbConfig module class.
 * @module MyDbConfig
 */
import {
    IDbConnection,
    IPoolClient,
    myDbRunMode
} from "./mydbImpl";
import {
    Node,
    NodeAPI
} from "node-red";
import {
    MyBaseModel
} from "@open-kappa/node-red-contrib-myutils";


/**
 * @summary The DB configuration.
 */
class MyDbConfig
    extends MyBaseModel
{
    private host: string;
    private port: string;
    private database: string;
    private ssl: boolean;
    private user: string;
    private password: string;
    private connection: null | IDbConnection;
    private idle: string;
    private min: string;
    private max: string;

    /**
     * @summary Constructor.
     * @param {NodeRed} nodeRed The nodeRed instance.
     * @param {Node} node The reference node-red node.
     * @param {Object} config The input configuration.
     */
    constructor(nodeRed: NodeAPI, node: Node, config: any)
    {
        super(nodeRed, node, config);

        this.host = config.host;
        this.port = config.port;
        this.database = config.database ?? "";
        this.ssl = config.ssl;
        this.user = "";
        this.password = "";
        this.idle = config.idle;
        this.min = config.min;
        this.max = config.max;

        const anyNode: any = node;
        if (typeof anyNode.credentials !== "undefined")
        {
            this.user = anyNode.credentials.user;
            this.password = anyNode.credentials.password;
        }

        this.connection = null;
    }

    /**
     * @summary Creates a connection to the DB.
     * @private
     * @return {IDbConnection} The connection class instance.
     */
    _makeConnection(): IDbConnection
    {
        const config = {
            "database": this.database,
            "host": this.host,
            "idleTimeoutMillis": this.idle,
            "max": this.max,
            "min": this.min,
            "password": this.password,
            "port": this.port,
            "ssl": this.ssl,
            "user": this.user
        };
        const Connection = myDbRunMode.getConnectionClass();
        return new Connection(config);
    }

    /**
     * @summary Connects and returns a client on which perform query()
     * and release().
     * @return {Promise<IPoolClient>} The pool client.
     */
    connect(): Promise<IPoolClient>
    {
        this.getNode().debug("Connecting to DB...");
        const self = this;
        if (self.connection === null)
        {
            self.connection = self._makeConnection();
        }

        return self.connection.connect();
    }

    /**
     * @summary Callback on node close events.
     * @param {boolean} isRemoved  True if the node is removed.
     */
    protected onCloseImpl(isRemoved: boolean): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        if (isRemoved)
        {
            // do something
        }
        else
        {
            // do something
        }
        return Promise.resolve();
    }
}

export {
    MyDbConfig
};

export default MyDbConfig;
