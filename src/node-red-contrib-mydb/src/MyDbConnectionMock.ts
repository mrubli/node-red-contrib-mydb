/**
 * The MyDbConnectionMock module class.
 * @module MyDbConnectionMock
 */
import {
    IDbConnection,
    IPoolClient
} from "./mydbImpl";


/**
 * @summary Class used to mock a connection to a DB during testing.
 */
class MyDbConnectionMock
implements IDbConnection, IPoolClient
{
    /**
     * @summary Constructor.
     * @param {json} _config The connection configuration.
     */
    // eslint-disable-next-line no-unused-vars
    constructor(_config: any)
    {
        // ntd
    }

    /**
     * @summary Mocks to connect to the DB.
     * As client instance to be used for the DB operations, it return itself.
     * @return {Promise<MyDbConnectionMock>} The client instance.
     */
    // eslint-disable-next-line class-methods-use-this
    connect(): Promise<IPoolClient>
    {
        return Promise.resolve(this);
    }

    /**
     * @summary Mocks a query.
     * @param {string} command The SQL command to execute.
     * @return {Promise<json>} A promise with the query result.
     */
    // eslint-disable-next-line no-unused-vars, class-methods-use-this
    query(command: string): Promise<any>
    {
        if (typeof command !== "string")
        {
            throw new Error("command must be a string");
        }
        return Promise.resolve({
            "command": command,
            "fields": [],
            // Undocumented on purpose:
            "isTesting": true,
            "oid": 123456,
            "rowCount": 0,
            "rows": []
        });
    }

    /**
     * @summary Mocks the release of the client.
     * @return {Promise<void>} A promise to wait the completion.
     */
    release(): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return Promise.resolve();
    }
}


export {
    MyDbConnectionMock
};

export default MyDbConnectionMock;
