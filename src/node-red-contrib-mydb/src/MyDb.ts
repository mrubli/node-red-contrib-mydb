/**
 * The MyDb module class.
 * @module MyDB
 */
import {
    IPoolClient,
    MyDbConfig
} from "./mydbImpl";
import type {
    Node,
    NodeAPI
} from "node-red";
import mustache from "mustache";
import handlebars from "handlebars";
import {
    MyBaseModel
} from "@open-kappa/node-red-contrib-myutils";


// Allowable template styles
type Style = "mustache" | "handlebars";
// Default template style used if the configuration does not specify one
const DefaultStyle: Style = "mustache";     // for backwards-compatibility with previous versions


/**
 * @summary The actual DB worker.
 */
class MyDb
    extends MyBaseModel
{
    private myDbConfig: null | MyDbConfig;
    private topic: string;
    private style: Style;
    private substEnvVars: boolean;
    private query: string;

    /**
     * @summary Constructor.
     * @param {NodeRed} nodeRed The nodeRed instance.
     * @param {Node} node The reference node-red node.
     * @param {Object} config The input configuration.
     */
    constructor(nodeRed: NodeAPI, node: Node, config: any)
    {
        super(nodeRed, node, config);
        this.topic = config.topic;
        this.myDbConfig = null;
        this.query = config.query;
        if (typeof config.mydbConfig !== "undefined")
        {
            const dbConfigNode = nodeRed.nodes.getNode(config.mydbConfig);
            if (typeof dbConfigNode !== "undefined"
                && dbConfigNode !== null)
            {
                const anyNode: any = dbConfigNode;
                this.myDbConfig = anyNode.dataModel;
            }
        }
        this.style = config.style || DefaultStyle;
        this.substEnvVars = typeof config.substEnvVars === "boolean" ? config.substEnvVars : false;
    }

    /**
     * @summary Callback on node input events.
     * @param msg {json} The input node-red message.
     */
    protected onInputImpl(msg: any): Promise<any>
    {
        const self = this;
        self.getNode().debug("onInput() called");
        if (self.myDbConfig === null)
        {
            throw new Error("Missing configuration node for MyDB");
        }
        let client: null | IPoolClient = null;
        const ret = msg;

        function renderTemplate(poolClient: IPoolClient): Promise<string>
        {
            client = poolClient;
            const view = {
                "msg": msg
            };
            try
            {
                var query = self.query;

                // Substitute environment variables if enabled
                query = self.maybeSubstEnvVars(query, msg);

                // Render the query using the current template style
                query = self.render(query, view);

                return Promise.resolve(query);
            }
            catch (ex)
            {
                return Promise.reject(ex);
            }
        }

        function doQuery(query: string): Promise<any>
        {
            self.getNode().debug(`Performed query: ${query}`);
            if (client === null)
            {
                throw new Error("internal bug");
            }
            return client.query(query);
        }

        function setResult(result: any): Promise<void>
        {
            ret.payload = result;
            return Promise.resolve();
        }

        function disconnect(): Promise<void>
        {
            if (client === null)
            {
                throw new Error("Internal bug");
            }
            return client.release();
        }

        function doCleanup(): Promise<any>
        {
            self.status({});
            return Promise.resolve(ret);
        }

        function onError(error: Error): Promise<any>
        {
            self.status(
                {
                    "fill": "red",
                    "shape": "ring",
                    "text": "Error"
                }
            );
            return Promise.reject(error);
        }

        self.status(
            {
                "fill": "blue",
                "shape": "dot",
                "text": "Running"
            }
        );

        return self.myDbConfig.connect()
            .then(renderTemplate)
            .then(doQuery)
            .then(setResult)
            .then(disconnect)
            .then(doCleanup)
            .catch(onError);
    }

    /**
     * @summary Callback on node close events.
     * @param isRemoved {boolean} True when node is removed.
     */
    protected onCloseImpl(isRemoved: boolean): Promise<void>
    {
        const self = this;
        if (isRemoved)
        {
            // do something
        }
        else
        {
            // do something
        }

        self.status({});
        return Promise.resolve();
    }

    private maybeSubstEnvVars(query: string, msg: any): string
    {
        const self = this;

        if (self.substEnvVars)
        {
            // Replace environment variables
            const newQuery = self.getNodeRed().util.evaluateNodeProperty(query, 'env', self.getNode(), msg);
            if (newQuery != query)
                self.getNode().debug(`Query before env-evaluation: ${query}`);
            return newQuery;
        }
        else
        {
            // Return the query as-is
            return query;
        }
    }

    /**
     * @summary Renders a query using the given data and the currently configured template style.
     * @param query {string} The query string.
     * @param view {json} The view/values to be used by the template engine.
     */
    private render(query: string, view: any): string
    {
        const self = this;

        self.getNode().debug(
            `Rendering ${self.style} query '${query}'`
            // + ` with ${JSON.stringify(view)}`
        );

        const engines: Record<Style, (query: string, view: object) => string> = {
            "mustache":
                function(query: string, view: object) {
                    return mustache.render(query, view);
                },
            "handlebars":
                function(query: string, view: object) {
                    const template = handlebars.compile(query);
                    return template(view);
                },
        };
        const engine = engines[self.style];
        if (!engine)
            throw `Unknown template style '${self.style}'`;

        return engine(query, view);
    }
}


export {
    MyDb
};

export default MyDb;
