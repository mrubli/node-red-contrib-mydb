
interface IPoolClient
{
    query(command: string): Promise<any>;
    release(): Promise<void>;
}

export {
    // eslint-disable-next-line no-undef
    IPoolClient
};

// eslint-disable-next-line no-undef
export default IPoolClient;
