import {IPoolClient} from "./mydbImpl";


interface IDbConnection
{
    connect(): Promise<IPoolClient>;
}

export {
    // eslint-disable-next-line no-undef
    IDbConnection
};

// eslint-disable-next-line no-undef
export default IDbConnection;
