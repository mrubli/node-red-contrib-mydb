/**
 * The MyDbRunMode module class.
 * This module exports the singleton instance directly.
 * @module MyDbRunMode
 */
import {
    MyDbConnectionMock
} from "./mydbImpl";
import pg from "pg";

const PgPool = pg.Pool;

/**
 * @summary Singleton for global running configuration.
 * This module exports the singleton instance directly.
 */
class MyDbRunMode
{
    private _isTesting: boolean;

    /**
     * @summary Constructor.
     */
    constructor()
    {
        this._isTesting = false;
    }

    /**
     * @summary Sets whether it is running in regression tests.
     * Therefore, it sets whether to use mock classes.
     * @param {boolean} isTesting True if it is testing.
     */
    setTesting(isTesting: boolean): void
    {
        this._isTesting = isTesting;
    }

    /**
     * @summary Gets the isTesting flag.
     * @return {boolean} True if it is testing.
     */
    isTesting(): boolean
    {
        return this._isTesting;
    }

    /**
     * @summary Returns the class to use for instantiating new DB connections.
     * If isTesting holds, returns MyDbConnectionMock, otherwise returns the
     * actual DB connection class.
     * @return {Class} The connection class.
     */
    getConnectionClass(): any
    {
        if (this._isTesting) return MyDbConnectionMock;
        return PgPool;
    }
}

const myDbRunMode = new MyDbRunMode();

export {
    myDbRunMode
};

export default myDbRunMode;
