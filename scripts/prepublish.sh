#!/bin/bash

function get_script_dir()
{
    local DIR=""
    local SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
        DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done
    THIS_SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
}
get_script_dir

function check_branch()
{
    local BRANCH_NAME="$(git rev-parse --abbrev-ref HEAD)"
    if [ "$BRANCH_NAME" != "master" ]; then
        echo "Error: not master branch"
        exit 1
    fi
    local BRANCH_TAG="$(git tag --points-at HEAD)"
    if [ "$BRANCH_TAG" == "" ]; then
        echo "Error: missing tag"
        exit 1
    fi
    grep "version" package.json | grep $BRANCH_TAG &> /dev/null
    local GREP_RES="$?"
    if [ "$GREP_RES" != "0" ]; then
        echo "Error: tag '$BRANCH_TAG' not matched in package.json"
        exit 1
    fi
    echo "Checks success!"
}

function test_install
{
    local IS_SOFT="$1"
    if [ "$IS_SOFT" = "OFF" ]; then
        rm -fr node_modules
    fi
    npm install .
    if [ "$?" != "0" ]; then
        echo "Install failed"
        exit 1
    fi
    npm audit --fix
    echo "Install success!"
}

function build()
{
    rm -fr dist
    npm run build
    if [ "$?" != "0" ]; then
        echo "Build failed"
        exit 1
    fi
    echo "Build success!"
}

IS_SOFT="OFF"
if [ "$1" = "--soft" ]; then
    IS_SOFT="ON"
elif [ "$1" = "--hard" ]; then
    IS_SOFT="OFF"
fi

cd $THIS_SCRIPT_DIR/..
check_branch
test_install $IS_SOFT
build

# EOF
